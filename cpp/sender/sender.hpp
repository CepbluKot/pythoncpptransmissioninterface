#include "json.hpp"
#include <string.h>
#include "../../baseInterfaces/cpp/client/client.hpp"


class Sender
{
    public:
        Sender(std::string pathToConfig)
        {
            this->sender = new Client(pathToConfig); 
            this->sender->connectToServer();
        }
        ~Sender();

        void sendJson(const json message)
        {
            this->buff = message.dump();
            
            const int length = this->buff.length();
            char* char_array = new char[length + 1];
            strcpy(char_array, this->buff.c_str());
            
            this->sender->sendToServer(char_array);
        }
    private:
        Client* sender;
        std::string buff;
};