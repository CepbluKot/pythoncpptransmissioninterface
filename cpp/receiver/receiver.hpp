#include <string>
#include "json.hpp"
#include "../../baseInterfaces/cpp/server/server.hpp"


using json = nlohmann::json;


class Receiver
{
    public:
        Receiver(std::string pathToConfig)
        {
            this->receiver = new Server(pathToConfig);
            this->receiver->connect(); 
        }
        ~Receiver();

        void receiveJson(void (*onReceive)(char buff[]))
        {
            // void* parseReceive = [](char buff[])
            // {
            //     auto receivedData = json::parse(buff);
            //     onReceive(receivedData);
            // };

            this->receiver->serverJob(onReceive);
        }
    private:
        Server* receiver;

};