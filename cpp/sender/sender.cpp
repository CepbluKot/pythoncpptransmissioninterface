#include<chrono>
#include<thread>
#include <iostream>
#include "sender.hpp"
#include "json.hpp"


using json = nlohmann::json;


int main()
{
    json j;
    j["pi"] = 3.141;
    j["happy"] = true;
    j["name"] = "Niels";

    Sender *snd = new Sender("/home/oleg/Documents/pythoncpptransmissioninterface/config.json");

    while(true)
    {
        try
        {
            std::cout.flush();
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            std::cout << "sent" << std :: endl;
            snd->sendJson(j);
        }
        catch(const std::exception& e)
        {
            std::cout << e.what() << '\n';
        }
        
    }
}