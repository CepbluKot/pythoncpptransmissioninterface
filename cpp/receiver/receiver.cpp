#include <iostream>
#include "receiver.hpp"
#include "json.hpp"


using json = nlohmann::json;


void onRecv(char* data)
{
    auto parsedData = json::parse(data);
    std::cout << parsedData <<std::endl;
}


int main()
{
    Receiver* rcv = new Receiver("/home/oleg/Documents/pythoncpptransmissioninterface/config.json");
    rcv->receiveJson(onRecv);
    
}