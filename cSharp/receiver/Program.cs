using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


class BaseServer
{
    class Config
    {
        [JsonProperty("port")]
        public int port { get; set; }

        [JsonProperty("nSymbols")]
        public int nSymbols { get; set; }
    }


    public BaseServer(string pathToConfig)
    {

        using (StreamReader r = new StreamReader(pathToConfig))
        {
            string json = r.ReadToEnd();
            configs = JsonConvert.DeserializeObject<Config>(json);
        }
        
        IPHostEntry host = Dns.GetHostEntry("localhost");
        IPAddress ipAddress = host.AddressList[0];
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, configs.port);

        listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        listener.Bind(localEndPoint);
    }


    public void connect()
    {
        listener.Listen(10);
        handler = listener.Accept();
    }

    public void serverJob(Func<string, int> onReceive)
    {
        while (true)
        {
            string data = null;
            byte[] bytes = null;

            bytes = new byte[configs.nSymbols];
            int bytesRec = handler.Receive(bytes);


            if (bytesRec != 0)
            {
                data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                onReceive(data);
            }
            else
            {
                handler.Shutdown(SocketShutdown.Both);
                this.connect();
            }
        }
    }

    private Socket listener;
    private Socket handler;
    private Config configs;
}


class Receiver
{

    public class ReceivedData
    {
        [JsonProperty("centerX")]
        public float centerX { get; set; }

        [JsonProperty("centerY")]
        public float centerY { get; set; }
    
        [JsonProperty("diameterInMM")]
        public float diameterInMM { get; set; }

        [JsonProperty("bienie")]
        public float bienie { get; set; }
    }


    public Receiver(string pathToConfig)
    {
        receiver = new BaseServer(pathToConfig);
        receiver.connect();
    }

    public void onReceiveJson(Func<ReceivedData, int> onReceive)
    {
        
        int parseStrToJson(string data)
        {
            ReceivedData parsed = JsonConvert.DeserializeObject<ReceivedData>(data);
            onReceive(parsed);

            return 0; // костыль
        }

        receiver.serverJob(parseStrToJson);
    }
    
    private BaseServer receiver;
}



public class SocketClient
{
    
    
    public static int Main(String[] args)
    {
        int smth(Receiver.ReceivedData data)
        {
            Console.WriteLine(" " + data.centerX + " " + data.centerY + " " + data.diameterInMM + data.bienie);
            return 0;
        }
    
        Receiver receiver = new Receiver("/home/oleg/Documents/pythoncpptransmissioninterface/config.json");

        receiver.onReceiveJson(smth);

        return 0;
    }

    

}