using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// Client app is the one sending messages to a Server/listener.
// Both listener and client can send messages back and forth once a
// communication is established.



class BaseClient
{
    class Config
    {
        [JsonProperty("port")]
        public int port { get; set; }

        [JsonProperty("nSymbols")]
        public int nSymbols { get; set; }
    }


    public BaseClient(string pathToConfig)
    {

        using (StreamReader r = new StreamReader(pathToConfig))
        {
            string json = r.ReadToEnd();
            configs = JsonConvert.DeserializeObject<Config>(json);
        }



        IPHostEntry host = Dns.GetHostEntry("localhost");
        IPAddress ipAddress = host.AddressList[0];
        remoteEP = new IPEndPoint(ipAddress, configs.port);

        // Create a TCP/IP  socket.
        sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
    }

    public void connect()
    {
        sender.Connect(remoteEP);
    }

    public void send(byte[] msg)
    {
        try
        {
            sender.Send(msg);
        }
        catch (Exception ex)
        {
        }
    }

    public void disconnect(string msg)
    {
        sender.Shutdown(SocketShutdown.Both);
        sender.Close();
    }


    private Socket sender;
    IPEndPoint remoteEP;
    private Config configs;
}


class Sender
{
    public Sender(string pathToConfig)
    {
        sender = new BaseClient(pathToConfig);
        sender.connect();
    }

    public void sendJsonString(string message)
    {
        // string jsonToStr = message.ToString();
        byte[] strToBytes = Encoding.ASCII.GetBytes(message);
        sender.send(strToBytes);
    }

    private BaseClient sender;
}


class SendData
    {

        public float centerX;
        public float centerY;
        public float diameterInMM;
        public float bienie;

    }



public class SocketClient
{
    public static int Main()
    {

    
    SendData toSend = new SendData();
    toSend.diameterInMM = 210;
    toSend.bienie = 31;
    toSend.centerY = 320;
    toSend.centerX = 1110;


    Sender sender = new Sender("/home/oleg/Documents/pythoncpptransmissioninterface/config.json");
    
    while (true)
    {
        System.Threading.Thread.Sleep(1000);
        
        sender.sendJsonString(JsonConvert.SerializeObject(toSend));    
    }
    
        return 0;
    }

}