import socket, json


class Client:
    def __init__(self, pathToConfig: str) -> None:
        f = open(pathToConfig)
        config = json.load(f)
        port = config['port']
        ip = '127.0.0.1'
        
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ip, port))

    def send(self, msgInBytes):
        self.s.sendall(msgInBytes)
