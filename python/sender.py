import json
from baseInterfaces.python.client.client import Client


class Sender:
    def __init__(self, pathToConfig: str) -> None:
        self.__client = Client(pathToConfig)


    def sendJson(self, data: dict):
        parsed = json.dumps(data)
        self.__client.send(parsed.encode())
