import json
from typing import Callable
from baseInterfaces.python.server.server import Server


class Receiver:
    def __init__(self, pathToConfig: str) -> None:
        self.__server = Server(pathToConfig)
        self.__server.connect()

    def receiveJson(self, onReceive: Callable[[], dict]):
        '''!!! launch in separate thread'''
        def whenReceived(received: str):
            parsed = json.loads(received)
            onReceive(parsed)

        self.__server.serverJob(whenReceived)
